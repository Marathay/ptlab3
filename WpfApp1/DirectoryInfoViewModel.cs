﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using WpfApp1.Models;

namespace WpfApp1
{
    public class DirectoryInfoViewModel : FileSystemInfoViewModel
    {
        private bool _isNodeExpanded = true;

        private bool sorting = true;

        private ObservableCollection<FileSystemInfoViewModel> _items = new ObservableCollection<FileSystemInfoViewModel>();

        public DirectoryInfoViewModel(object owner) : base(owner)
        {
        }

        public ObservableCollection<FileSystemInfoViewModel> Items { 
            get => _items;
            private set
            {
                _items = value;
                NotifyPropertyChanged();
            } 
        }

        public bool IsNodeExpanded
        {
            get => _isNodeExpanded;
            set
            {
                _isNodeExpanded = value;
                NotifyPropertyChanged();
            }
        }

        public FileSystemWatcher Watcher;

        public void InitWatchers()
        {
            Watcher = new FileSystemWatcher(Path);
            Watcher.Created += OnFileSystemChanged;
            Watcher.Renamed += OnFileSystemChanged;
            Watcher.Deleted += OnFileSystemChanged;
            Watcher.Changed += OnFileSystemChanged;
            Watcher.EnableRaisingEvents = true;
        }

        public bool OpenDataTree(string path)
        {
            Path = path;
            bool result = false;
            result = LoadDataTree(path, result);
            InitWatchers();

            return result;
        }

        private bool LoadDataTree(string path, bool result)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() => Items.Clear()));

            foreach (var dirName in Directory.GetDirectories(path))
            {
                var dirInfo = new DirectoryInfo(dirName);
                DirectoryInfoViewModel itemViewModel = new DirectoryInfoViewModel(this);
                Items_CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, itemViewModel));
                itemViewModel.Model = dirInfo;
                itemViewModel.Size = dirInfo.EnumerateFileSystemInfos().Count();
                itemViewModel.OpenDataTree(dirInfo.FullName);
                Application.Current.Dispatcher.BeginInvoke(new Action(() => Items.Add(itemViewModel)));
            }

            foreach (var fileName in Directory.GetFiles(path))
            {
                var fileInfo = new FileInfo(fileName);
                FileInfoViewModel itemViewModel = new FileInfoViewModel( this );
                Items_CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, itemViewModel));
                itemViewModel.StatusMessage = $"Loading files from directory {fileInfo.Directory}";
                itemViewModel.Model = fileInfo;
                itemViewModel.Size = fileInfo.Length;
                itemViewModel.parentDirName = fileInfo.Directory.ToString();
                Application.Current.Dispatcher.BeginInvoke(new Action(() => Items.Add(itemViewModel)));
            }

            return true;
        }
        

        private void WatcherError(object sender, ErrorEventArgs e)
        {
            MessageBox.Show("Wystąpił błąd watchera: " + e.GetException().Message);
        }

        private void OnFileSystemChanged(object sender, FileSystemEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                IsNodeExpanded = true;
                LoadDataTree(Path, false);
            });
        }

        public async Task SortAsync(SortingModel sortingModel, CancellationToken ct)
        {
            var sorted = Items.OrderByDescending(o => (o.Model.Attributes & FileAttributes.Directory) != 0);
            Func<FileSystemInfoViewModel, IComparable> selector;
            switch (sortingModel.SortingOptions)
            {
                case SortingOptions.Name:
                    selector = o => o.Model.Name;
                    break;
                case SortingOptions.Created:
                    selector = o => o.Model.CreationTime;
                    break;
                case SortingOptions.Modified:
                    selector = o => o.Model.LastWriteTime;
                    break;
                case SortingOptions.Size:
                    selector = o => o.Size;
                    break;
                case SortingOptions.Extension:
                    selector = o => o.Model.Extension;
                    break;
                default:
                    selector = o => o.Model.Name;
                    break;
            }
            if (sortingModel.SortingDirection.Equals(SortingDirection.Descending))
            {
                sorted = sorted.ThenByDescending(selector);
            }
            else
            {
                sorted = sorted.ThenBy(selector);
            }
            Items = new ObservableCollection<FileSystemInfoViewModel>(sorted);
            var TaskList = new ObservableCollection<Task>();
            foreach (var item in Items)
            {  
                if(sorting)
                {
                    ct.Register(() => CancellSorting()
                    );
                    Items_CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
                    item.StatusMessage = $"Sorting files from directory {item.parentDirName}";
                    if (item.GetType() == typeof(DirectoryInfoViewModel))
                    {
                        var LastTask = new Task(() =>
                        {
                            (item as DirectoryInfoViewModel)?.SortAsync(sortingModel, ct);
                        }, TaskCreationOptions.PreferFairness);

                            // implementation for 4.2
                            //var LastTask = new Task(() =>
                            //{
                            //    (item as DirectoryInfoViewModel)?.Sort(sortingModel);
                            //}, TaskCreationOptions.LongRunning);

                            // old implementation with out TaskCreationOptions z zadania 4.2
                            // uncomment this to confirm 4.4 task
                            //var LastTask = new Task(() => (item as DirectoryInfoViewModel)?.Sort(sortingModel));
                            //LastTask.Start();
                            //TaskList.Add(LastTask);
                    };
                }
            }
            Task.WaitAll(TaskList.ToArray());

                // old implementation to compare better performance with multiple threads
                //foreach (var item in Items)
                //{
                //    Items_CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
                //    item.StatusMessage = $"Sorting files from directory {item.parentDirName}";
                //    (item as DirectoryInfoViewModel)?.Sort(sortingModel);
                //}
                StatusMessage = "Ready";            
        }

        private void CancellSorting()
        {
            sorting = false;
        }

        private void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            switch (args.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var item in args.NewItems.Cast<FileSystemInfoViewModel>())
                    {
                        item.PropertyChanged += Item_PropertyChanged;
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (var item in args.NewItems.Cast<FileSystemInfoViewModel>())
                    {
                        item.PropertyChanged -= Item_PropertyChanged;
                    }
                    break;
            }
        }

        private void Item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "StatusMessage" && sender is FileSystemInfoViewModel viewModel)
                this.StatusMessage = viewModel.StatusMessage;
        }
    }
}