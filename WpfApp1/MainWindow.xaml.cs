﻿using DataAccess;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly P2PDbContext _context =
            new P2PDbContext();

        private FileExplorer _fileExplorer;
        public static int mainWindowThread;

        // todo clean this class
        public MainWindow()
        {
            mainWindowThread = Thread.CurrentThread.ManagedThreadId;
            Debug.WriteLine("Main ui thread " + mainWindowThread);

            _context.Database.EnsureCreated();
            _fileExplorer = new FileExplorer();
            DataContext = _fileExplorer;
            _fileExplorer.PropertyChanged += FileExplorer_PropertyChanged;
            _fileExplorer.OpenTextFileEvent += (o, txt) => textBlock.Text = txt;
            InitializeComponent();
        }

        private void FileExplorer_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(FileExplorer.Lang))
                CultureResources.ChangeCulture(CultureInfo.CurrentUICulture);
        }

        private void OnDirDelete(object sender, RoutedEventArgs e)
        {
            var node = (treeView.SelectedItem as DirectoryInfoViewModel).Model;
            try
            {
                Directory.Delete(node.FullName);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void OnCreateNewFile(object sender, RoutedEventArgs e)
        {
            var directoryInfoViewModel = (treeView.SelectedItem as DirectoryInfoViewModel);
            var filePath = directoryInfoViewModel.Model.FullName;
            Dialog dialog = new Dialog(filePath);
            dialog.ShowDialog();
        }

        private void OnFileDelete(object sender, RoutedEventArgs e)
        {
            var item = treeView.SelectedItem as FileInfoViewModel;
            var node = item.Model;
            try
            {
                File.Delete(node.FullName);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                return;
            }
        }

        private void OnFileOpen_Click(object sender, RoutedEventArgs e)
        {
            var filePath = (treeView.SelectedItem as FileInfoViewModel).Model.FullName;
            using (var textReader = File.OpenText(filePath))
            {
                textBlock.Text = textReader.ReadToEnd();
            }
        }

        private void MenuItem_ExitApp(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void treeView_MouseUp(object sender, MouseButtonEventArgs e)
        {
        }

        private void TreeView_OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            _fileExplorer.SelectedItem(treeView.SelectedItem);
            if (treeView.SelectedItem is FileSystemInfoViewModel)
            {
                var treeViewSelectedItem = (treeView.SelectedItem as FileSystemInfoViewModel);
                string status = "";
                var item = treeViewSelectedItem.Model;
                if (item is FileSystemInfo)
                {
                    var attr = item.Attributes;
                    if ((attr & FileAttributes.System) != 0) status += "S";
                    else status += "-";
                    if ((attr & FileAttributes.Archive) != 0) status += "A";
                    else status += "-";
                    if ((attr & FileAttributes.Directory) != 0) status += "D";
                    else status += "-";
                    if ((attr & FileAttributes.Hidden) != 0) status += "H";
                    else status += "-";
                }

                statusText.Text = status;
            }
            else
            {
                statusText.Text = treeView.SelectedItem.GetType().ToString();
            }
        }        
    }

    public class CultureResources
    {
        public Strings GetStringsInstance()
        {
            return new Strings();
        }

        private static ObjectDataProvider _provider;

        public static ObjectDataProvider ResourceProvider
        {
            get
            {
                if (_provider == null)
                    _provider =
                        (ObjectDataProvider) System.Windows.Application.Current.FindResource("Strings");
                return _provider;
            }
        }

        public static void ChangeCulture(CultureInfo culture)
        {
            ResourceProvider.Refresh();
        }

    }
}