﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using WpfApp1.Annotations;

namespace WpfApp1
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public delegate void eh(object sender, PropertyChangedEventArgs e);

        public Exception Exception;
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class FileSystemInfoViewModel : ViewModelBase
    {
        private FileSystemInfo model;
        private string statusMessage;
        public string Path;
        public string _imagePath;
        public string parentDirName;

        public object Owner { get; set; }

        public FileSystemInfoViewModel(object owner)
        {
            this.Owner = owner;
        }

        public FileExplorer OwnerExplorer
        {
            get
            {
                var owner = Owner;
                while (owner is DirectoryInfoViewModel directoryInfoViewModel)
                {
                    if (directoryInfoViewModel.Owner is FileExplorer explorer)
                    {
                        return explorer;
                    }

                    owner = directoryInfoViewModel.Owner;
                }

                return null;
            }
        }

        public FileSystemInfo Model
        {
            get => model;
            set
            {
                if (model == value) return;
                model = value;
                ImagePath = SelectProperIcon(value);
                NotifyPropertyChanged();
            }
        }

        public string ImagePath
        {
            get => _imagePath;
            set
            {
                if (_imagePath == value) return;
                _imagePath = value;
                NotifyPropertyChanged();
            }
        }

        public long Size { get; set; }

        public string StatusMessage
        {
            get { return statusMessage; }
            set
            {
                if (value != null)
                {
                    statusMessage = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private static string SelectProperIcon(FileSystemInfo info)
        {
            if (info is DirectoryInfo)
            {
                return "/images/folder_closed.png";
            }

            return info.Extension.ToLower() switch
            {
                ".pdf" => "/images/pdf.png",
                ".txt" => "/images/text_file.png",
                ".png" => "/images/picture.png",
                ".jpg" => "/images/picture.png",
                _ => "/images/document.png"
            };
        }
    }
}