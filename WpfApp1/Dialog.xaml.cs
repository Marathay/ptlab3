﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for Dialog.xaml
    /// </summary>
    public partial class Dialog : Window
    {
        public string directoryPath;

        public Dialog(string dirPath)
        {
            this.directoryPath = dirPath;
            InitializeComponent();
        }

        private void HiddenCheck_Checked(object sender, RoutedEventArgs e)
        {
        }

        private void NameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string name = NameTextBox.Text;
            bool isFile = (bool) FileRadioButton.IsChecked;
            bool ReadOnly = (bool) ReadCheck.IsEnabled;
            bool Archive = (bool) ArchiveCheck.IsEnabled;
            bool Hidden = (bool) HiddenCheck.IsEnabled;
            bool SystemC = (bool) SystemCheck.IsEnabled;

            var fullPath = System.IO.Path.Combine(this.directoryPath, name);

            if (validateFileShouldNotExist(fullPath)) return;

            if (isFile)
            {
                if (validateFilename(name)) return;
                createFile(fullPath, ReadOnly, Archive, Hidden, SystemC);
            }
            else
            {
                if (validateInvalidDirChars(name)) return;
                var dir_attr = createDirectory(fullPath, ReadOnly, Archive, Hidden, SystemC);
            }

            this.Close();
        }

        private static bool validateFileShouldNotExist(string fullPath)
        {
            if (System.IO.File.Exists(fullPath))
            {
                MessageBox.Show("File with this name already exists");
                return true;
            }

            if (System.IO.Directory.Exists(fullPath))
            {
                MessageBox.Show("Directory with this name already exists");
                return true;
            }

            return false;
        }

        private bool validateFilename(string testName)
        {
            string strTheseAreInvalidFileNameChars = new string(System.IO.Path.GetInvalidFileNameChars());
            Regex regInvalidFileName = new Regex("[" + Regex.Escape(strTheseAreInvalidFileNameChars) + "]");

            if (regInvalidFileName.IsMatch(testName))
            {
                MessageBox.Show(testName + " has forbidden chars");
                return true;
            }

            if (!System.IO.Path.HasExtension(testName))
            {
                MessageBox.Show(testName + " does not have extension");
                return true;
            }

            return false;
        }

        public bool validateInvalidDirChars(string dirName)
        {
            char[] reserved = System.IO.Path.GetInvalidFileNameChars();

            foreach (char c in reserved)
            {
                if (dirName.Contains(c))
                {
                    MessageBox.Show(dirName + " has forbidden chars");
                    return true;
                }
            }

            return false;
        }

        private static DirectoryInfo createDirectory(string fullPath, bool ReadOnly, bool Archive, bool Hidden,
            bool SystemC)
        {
            System.IO.Directory.CreateDirectory(fullPath);
            var dir_attr = new System.IO.DirectoryInfo(fullPath);
            if (ReadOnly)
            {
                dir_attr.Attributes |= System.IO.FileAttributes.ReadOnly;
            }

            if (Archive)
            {
                dir_attr.Attributes |= System.IO.FileAttributes.Archive;
            }

            if (Hidden)
            {
                dir_attr.Attributes |= System.IO.FileAttributes.Hidden;
            }

            if (SystemC)
            {
                dir_attr.Attributes |= System.IO.FileAttributes.System;
            }

            return dir_attr;
        }

        private static void createFile(string fullPath, bool ReadOnly, bool Archive, bool Hidden, bool SystemC)
        {
            var file = System.IO.File.Create(fullPath);
            if (ReadOnly)
            {
                System.IO.File.SetAttributes(fullPath, System.IO.FileAttributes.ReadOnly);
            }

            if (Archive)
            {
                System.IO.File.SetAttributes(fullPath, System.IO.FileAttributes.Archive);
            }

            if (Hidden)
            {
                System.IO.File.SetAttributes(fullPath, System.IO.FileAttributes.Hidden);
            }

            if (SystemC)
            {
                System.IO.File.SetAttributes(fullPath, System.IO.FileAttributes.System);
            }

            file.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}