﻿using System.Windows;
using WpfApp1.Models;

namespace WpfApp1
{
    public partial class SortDialog : Window
    {
        public SortDialog(SortingModel model)
        {
            DataContext = model;
            InitializeComponent();
        }
    }
}