﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using WpfApp1.Models;

namespace WpfApp1
{
    public class FileExplorer : ViewModelBase, INotifyPropertyChanged
    {
        private DirectoryInfoViewModel _root;
        private object _selectedItem;
        private string _statusMessage;
        private int _maxThread = MainWindow.mainWindowThread;
        public Visibility _btnVis = Visibility.Hidden;
        public CancellationTokenSource tokenSource;


        public FileExplorer()
        {
            CommandOpenRootDir = new RelayCommand(ExecuteOpenRootDirectoryAsync);
            CommandSort = new RelayCommand(ExecuteSort, CanExecuteSort);
            OpenTextFile = new RelayCommand(ExecuteOpenTextFile, CanExecuteOpenTextFile);
            CancellButton = new RelayCommand(CancellButtonClick);

            SortingModel = new SortingModel();
            SortingModel.PropertyChanged += (o, e) => _root.SortAsync(SortingModel, tokenSource.Token);
            NotifyPropertyChanged(nameof(Lang));
            tokenSource = new CancellationTokenSource();

        }

        public event EventHandler<string> OpenTextFileEvent;
        public RelayCommand CommandOpenRootDir { get; private set; }
        public RelayCommand CommandSort { get; private set; }
        public RelayCommand OpenTextFile { get; private set; }
        public SortingModel SortingModel { get; private set; }

        public RelayCommand CancellButton { get; private set; }


        public DirectoryInfoViewModel Root
        {
            get => _root;
            set
            {
                if (_root == value) return;
                _root = value;
                NotifyPropertyChanged();
            }
        }

        public string Lang
        {
            get { return CultureInfo.CurrentUICulture.TwoLetterISOLanguageName; }
            set
            {
                if (value != null)
                    if (CultureInfo.CurrentUICulture.TwoLetterISOLanguageName != value)
                    {
                        CultureInfo.CurrentUICulture = new CultureInfo(value);
                        NotifyPropertyChanged();
                    }
            }
        }

        public Visibility BtnVis
        {
            get { return _btnVis; }
            set
            {
                if (value != null)
                {
                    _btnVis = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public string StatusMessage
        {
            get { return _statusMessage; }
            set
            {
                if (value != null)
                {
                    _statusMessage = value;
                    NotifyPropertyChanged("StatusMessage");
                    Thread.Sleep(200);

                    if (_maxThread < Thread.CurrentThread.ManagedThreadId)
                    {
                        _maxThread = Thread.CurrentThread.ManagedThreadId;
                        Debug.WriteLine("Max thredId is " + Thread.CurrentThread.ManagedThreadId);
                    }
                    Debug.WriteLine("Current thredId is " + Thread.CurrentThread.ManagedThreadId);
                }
            }
        }

        public void OpenRoot(string path)
        {
            Root = new DirectoryInfoViewModel(this);
            Root.PropertyChanged += Root_PropertyChanged;
            Root.OpenDataTree(path);
            StatusMessage = "Ready";
            BtnVis = Visibility.Hidden;
        }

        public void SelectedItem(object treeViewSelectedItem)
        {
            _selectedItem = treeViewSelectedItem;
        }

        public void Root_PropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == "StatusMessage" && sender is FileSystemInfoViewModel viewModel)
                StatusMessage = viewModel.StatusMessage;
        }

        private bool CanExecuteOpenTextFile(object selectedItem)
        {
            return selectedItem is FileInfoViewModel fileModel &&
                   ((IList)new[] { ".txt", ".md", ".cs", ".csproj", }).Contains(fileModel.Model.Extension.ToLower());
        }

        private void ExecuteOpenTextFile(object selectedItem)
        {
            using (var textReader = File.OpenText((selectedItem as FileInfoViewModel)?.Model.FullName))
            {
                OpenTextFileEvent?.Invoke(this, textReader.ReadToEnd());
            }
        }

        private bool CanExecuteSort(object obj)
        {
            return _root != null;
        }

        private async void ExecuteSort(object obj)
        {
            BtnVis = Visibility.Visible;
            await Task.Factory.StartNew(() =>
            {
                Thread thread = new Thread(() =>
                {
                    var window = new SortDialog(SortingModel);
                    window.Show();
                    window.Closed += (sender2, e2) =>
                    window.Dispatcher.InvokeShutdown();
                    System.Windows.Threading.Dispatcher.Run();
                });
                    thread.SetApartmentState(ApartmentState.STA);
                    thread.Start();
            }, tokenSource.Token, TaskCreationOptions.PreferFairness, TaskScheduler.Default);
        }

        private void CancellButtonClick(object o)
        {
                BtnVis = Visibility.Hidden;
                this.tokenSource.Cancel();
        }

        private async void ExecuteOpenRootDirectoryAsync(object parameters)
        {
            var dlg = new FolderBrowserDialog
            {
                Description = Strings.SelectDirectory
            };
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                await Task.Factory.StartNew(() =>
                {
                    OpenRoot(dlg.SelectedPath);
                }, TaskCreationOptions.PreferFairness);

                // implementation for 4.2
                // }, TaskCreationOptions.LongRunning);
            }
        }
    }
}