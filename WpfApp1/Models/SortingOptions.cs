﻿namespace WpfApp1.Models
{
    public enum SortingOptions
    {
        Name,
        Created,
        Modified,
        Size,
        Extension
    }
}