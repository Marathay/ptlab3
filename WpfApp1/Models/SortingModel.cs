﻿namespace WpfApp1.Models
{
    public class SortingModel : ViewModelBase
    {
        private SortingOptions _sortingOptions = SortingOptions.Name;

        public SortingOptions SortingOptions
        {
            get => _sortingOptions;
            set
            {
                _sortingOptions = value;
                NotifyPropertyChanged();
            }
        }

        private SortingDirection _dir = SortingDirection.Ascending;

        public SortingDirection SortingDirection
        {
            get => _dir;
            set
            {
                _dir = value;
                NotifyPropertyChanged();
            }
        }
    }
}