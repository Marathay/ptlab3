﻿namespace WpfApp1.Models
{
    public enum SortingDirection
    {
        Ascending,
        Descending
    }
}