﻿using System.Windows.Input;

namespace WpfApp1
{
    public class FileInfoViewModel : FileSystemInfoViewModel
    {
        private bool _isNodeExpanded = false;
        public ICommand OpenTextFileCmd => OwnerExplorer.OpenTextFile;

        public FileInfoViewModel(FileSystemInfoViewModel owner): base(owner)
        {
            
        }

        public bool IsNodeExpanded
        {
            get => _isNodeExpanded;
            set
            {
                _isNodeExpanded = value;
                NotifyPropertyChanged();
            }
        }
    }
}