﻿namespace DataAccess.Models
{
    public class FilePermission
    {
        public enum PermissionType
        {
            CreateDirectories,
            CreateFiles,
            Delete,
            DeleteSubdirectoriesAndFiles,
            ExecuteFile,
            FullControl,
            ListDirectory,
            Modify,
            Read,
            ReadAndExecute,
            ReadAttributes,
            ReadData,
            Write,
            WriteAttributes,
            WriteData
        }

        public int Id { get; private set; }
        public PermissionType Permission { get; private set; }
        public int UserId { get; private set; }
        public virtual User User { get; private set; }
    }
}
