﻿using System;

namespace DataAccess.Models
{
    public class FileDetail
    {
        public int Id { get; private set; }
        public DateTime FileCreationDate { get; private set; }
        public int UserId { get; private set; }
        public virtual User User { get; private set; }
    }
}
