﻿namespace DataAccess.Models
{
    public class Notification
    {
        public int Id { get; private set; }
        public int UserId { get; private set; }
        public virtual User User { get; private set; }
        public int FilePermissionId { get; private set; }
        public virtual FilePermission FilePermission { get; private set; }
    }
}
