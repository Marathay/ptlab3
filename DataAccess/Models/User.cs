﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DataAccess.Models
{
    public class User
    {
        public int Id { get; private set; }
        public string Login { get; private set; }
        public string Password { get; private set; }
        public string  IpAddress { get; private set; }
        public  virtual ICollection<FileDetail> FileDetails { get; private set; } =
            new ObservableCollection<FileDetail>();
    }
}
