﻿using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;

namespace DataAccess
{
    public class P2PDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<FileDetail> FileDetails { get; set; } 
        public DbSet<FilePermission> FilePermissions { get; set; }

        protected override void OnConfiguring(
            DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source={Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "P2P.db")}");

            optionsBuilder.UseLazyLoadingProxies();
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FilePermission>()
                .Property(c => c.Permission)
                .HasConversion<int>();
        }
    }
}
